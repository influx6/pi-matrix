#!/usr/bin/python3
import yaml
import sys

with open(sys.argv[1], 'r') as f:
    try:
        data = yaml.load(f, Loader=yaml.FullLoader)
    except yaml.YAMLError as exc:
        print(exc)

hs_token = data['appservice']['hs_token']
as_token = data['appservice']['as_token']

with open(sys.argv[2], 'r') as infile:
    try:
        dataNew = yaml.load(infile, Loader=yaml.FullLoader)
        dataNew['appservice']['hs_token'] = hs_token
        dataNew['appservice']['as_token'] = as_token
#        yaml.dump(dataNew, f, default_flow_style=False)
    except yaml.YAMLError as exc:
        print(exc)

with open('outfile.yml', 'w') as outfile:
    try:
        yaml.dump(dataNew, outfile, default_flow_style=False)
    except yaml.YAMLError as exc:
        print(exc)
