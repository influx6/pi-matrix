#!/bin/bash

#docker stop $(docker ps -a -q)
#docker rm $(docker ps -a -q)

# check if set up before on this pi
#if [ ! -f configs ]; then
#    echo "enter user name"
#    read user
#    echo "user="$user > configs
#    echo "sharedSecret=`pwgen -s 128 1`" >> configs
#    echo "DB_PASSWORD=`pwgen -s 64 1`" >> configs
#fi
source configs

DOMAIN="$user.nova.chat"
BRIDGES=( "whatsapp" "facebook" "telegram" "hangouts" "manager")
SORUBRIDGES=( "slack" "instagram" "twitter" )

# install riot static files
sudo mkdir /srv/web
sudo mkdir /srv/web/im
wget -q https://sfo2.digitaloceanspaces.com/novasw/riot-v1.5.10-25-g35e1b85a.tar.gz
sudo tar -zxf riot-v1.5.10-25-g35e1b85a.tar.gz -C /srv/web/im --strip-components=1
sudo cp riot/config-template.json /srv/web/im/config.json
sudo sed -i 's/$(DOMAIN)/'"$DOMAIN"'/g' /srv/web/im/config.json
sudo chown -R ubuntu:ubuntu /srv/web
echo "riot installed in /im"

sudo sed -i 's/alaska1/'"$user"'pi/g' /etc/hostname
sudo sed -i 's/alaska1/'"$user"'pi/g' /etc/hosts

sudo chown -R 991:991 synapse/media/
sudo chown -R 991:991 synapse/uploads/

if [ ! -f docker-compose.yml ]; then
    cp docker-compose-template.yml docker-compose.yml
fi

# set up caddy
docker-compose up -d caddy


# set up db
if [ ! "$(docker ps | grep postgres)" ]; then
    echo "setting up db"
    sed -i 's/$(db_password)/'"$DB_PASSWORD"'/g' docker-compose.yml
    docker-compose up -d db
fi
echo "db up"

# set up synapse
if [ ! -f synapse/homeserver.yaml ]; then
    echo "setting up synapse"
    docker-compose run --rm -e SYNAPSE_SERVER_NAME=$DOMAIN -e SYNAPSE_REPORT_STATS=yes synapse generate 
    sudo bash -c "grep -E -- 'registration_shared_secret:|macaroon_secret_key:|signing_key_path:|form_secret:' synapse/homeserver.yaml > synapse/settings.yaml"
    sudo sed -i 's/: /=/g' synapse/settings.yaml 
    source synapse/settings.yaml
    sudo mv synapse/homeserver.yaml synapse/generated-homeserver.yaml
    sudo cp synapse/homeserver-template.yaml synapse/homeserver.yaml    
    sudo sed -i 's/$(db_password)/'"$DB_PASSWORD"'/g;s/$(domain)/'"$DOMAIN"'/g;s/$(registration_shared_secret)/'"$registration_shared_secret"'/g;s/$(macaroon_secret_key)/'"$macaroon_secret_key"'/g;s/$(form_secret)/'"$form_secret"'/g;s/$(sharedSecret)/'"$sharedSecret"'/g' synapse/homeserver.yaml
    docker-compose up -d synapse
fi

# set up bridges
for i in "${BRIDGES[@]}"
do
    if [ ! "$(docker exec -it postgres psql -P pager=off -U synapse -c "\l" | grep $i)" ]; then
	docker exec -it postgres psql -U synapse -c "CREATE DATABASE mautrix$i;"
        docker exec -it postgres psql -U synapse -c "GRANT ALL PRIVILEGES ON DATABASE mautrix$i TO synapse;"    
    else
	echo "$i already in DB"
    fi
    FILE=bridges/mautrix-$i/registration.yaml
    if [ -f $FILE ]; then
	echo "$i all set up"
    else
	echo "Starting $i container 1st time"
        sudo cp bridges/mautrix-$i/config-template.yaml bridges/mautrix-$i/config.yaml
        sudo sed -i 's/$(domain)/'"$DOMAIN"'/g;s/$(user)/'"$user"'/g;s/$(sharedSecret)/'"$sharedSecret"'/g;s/$(sorudomain)/'"$SORUDOMAIN"'/g;s/$(db_password)/'"$DB_PASSWORD"'/g' bridges/mautrix-$i/config.yaml
    	if [ "$i" == "manager" ]; then
		echo "manager, setting up"
		docker-compose up -d mautrix-$i
		break
	fi
	docker-compose up mautrix-$i
	sudo cp bridges/mautrix-$i/registration.yaml synapse/appservices/mautrix-$i.yaml  
	if grep -q "$i.yaml" synapse/homeserver.yaml; then
	    echo "registration already in HS"
	else
	    echo "modifying homeserver.yaml"
	    sudo sed -i '/app_service_config_files:/a   \ \- \/data\/appservices\/mautrix\-'$i'.yaml' synapse/homeserver.yaml
	    echo "restarting synapse and starting container"
	    sudo chmod +r synapse/appservices/mautrix-$i.yaml
    	    docker-compose restart synapse	
	    docker-compose up -d mautrix-$i
	fi
    fi
done



for i in "${SORUBRIDGES[@]}"
do
        FILE=synapse/appservices/mx-puppet-$i.yaml
        if [ -f $FILE ]; then
            echo "$FILE all set up"
        else
            echo "Setting up $i"
            sudo cp bridges/mx-puppet-$i/config-template.yaml bridges/mx-puppet-$i/config.yaml
	    sudo sed -i 's/$(domain)/'"$DOMAIN"'/g;s/$(user)/'"$user"'/g;s/$(sharedSecret)/'"$sharedSecret"'/g' bridges/mx-puppet-$i/config.yaml
            echo "Starting $i container 1st time"
            docker-compose up mx-puppet-$i
	    sudo sed -i 's/0\.0\.0\.0/mx\-puppet\-'"$i"'/g' bridges/mx-puppet-$i/$i-registration.yaml
            if grep -q "$i.yaml" synapse/homeserver.yaml; then
                echo "registration already in HS"
            else
                echo "modifying homeserver.yaml"
		sudo cp bridges/mx-puppet-$i/$i-registration.yaml synapse/appservices/mx-puppet-$i.yaml
                sudo sed -i '/app_service_config_files:/a   \ \- \/data\/appservices\/mx\-puppet\-'$i'.yaml' synapse/homeserver.yaml
                echo "restarting synapse and starting container"
                sudo chmod +r synapse/appservices/mx-puppet-$i.yaml
	        docker-compose restart synapse
		docker-compose up -d mx-puppet-$i
            fi
        fi
done


sed -i 's/#    restart/    restart/g' docker-compose.yml
docker-compose up -d
